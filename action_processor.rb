"""
The ActionProcessor has a single public function, `process_action`.  It takes in a
set of values and decided what to do with them.  This includes validating the vote and updating the DB.

It returns a string describing what happened.
"""
require 'json'
require './helpers'
class ActionProcessor
  include Helpers
  def initialize night, db_conn, time_period
    @night = night
    @db = db_conn
    @time_period = time_period
  end

  def run start_time = nil, end_time = nil
    if @time_period == 'D'
      start_time ||= day_start_timestamp @night
      end_time ||= day_end_timestamp @night
    else
      start_time ||= night_start_timestamp @night
      end_time ||= night_end_timestamp @night
    end
    Role.where(Sequel.like(:time_period, "%#{@time_period}%")).order(:priority).reverse.map do |role|
      role.actions.where(vote_time: start_time..end_time).sort{|a,b| a.vote_time <=> b.vote_time}.reverse.uniq{|action| action.player_id}.reverse.map{|action| process_action action}.join "\n"
    end.select{|a| a.size > 0}.join "\n"
  end
  
  def process_action action
    @action = action
    @user = Player.first(id_number: @action.player_id)

    ret = ""
    form = JSON.parse(@action.form)
    player_target_keys = form.keys.select{|key| key.include? "player-to-target"}
    player_target_ids = player_target_keys.map{|key| form[key]}
    player_targets = player_target_ids.map{|target| Player[target]}
    target_names = player_targets.map{|target| target.name}.join(", ")

    group_target_keys = form.keys.select{|key| key.include? "group-to-target"}
    group_target_ids = group_target_keys.map{|key| form[key]}
    group_targets = group_target_ids.map{|target| Group[target]}
    target_names += group_targets.map{|target| target.name}.join(", ")
    @targets = group_targets + player_targets

    if not @user
      return "Someone tried to act as a(n) #{@action.role_name} with id #{@action.player_id} targeting #{target_names}, but that's not a valid ID"
    end
    unless right_role
      return "#{@action.player.name} tried to act as a #{@action.role_name} targeting #{target_names}, but is actually a(n) #{@action.player.role.name}."
    end

    if not @user.current_status(@night).eligible and @action.role_name != "Mossad"
      ret += ineligible_reason
      return ret
    end

    @user.current_status(@night).update(acted: true)
    @night.upto 5 do |night|
      @user.current_status(@night).used += 1
      @user.current_status(@night).save
    end
    
    if @user.current_status(@night).bamboozled and @action.role_name != "Mossad"
      ret += "#{@user.name} originally targeted #{target_names} but was bumbled and is now targeting "
      new_player_targets = []
      player_target_ids.each do |target|
        target = get_random_player_target new_player_targets
        new_player_targets << target
      end

      new_group_targets = []
      group_target_ids.each do |target|
        new_group_targets << Group.where(Sequel.~(id: new_group_targets)).all.sample
      end
      @targets = new_player_targets.map{|a| Player[a]} + new_group_targets
      ret += @targets.map{|target| target.name}.join(", ")
      ret += ". #{@user.name} is no longer bumbled.\n"
      @user.player_statuses.each do |status|
        next if status.night <= @night
        status.bamboozled = false
        status.save
      end
    end

    if (@user.current_status(@night).laying_low or #tonight
        (@user.current_status(@night - 1) and
         @user.current_status(@night - 1).laying_low and
         @time_period == 'N')) and #last night
      @action.role_name != 'Mossad'
      target_names = @targets.map{|t| t.name}
      ret += "#{@user.name} tried to use the #{@action.role_name} against #{target_names} but they're laying low"
    end

    if @user.current_status(@night).action_cancelled and @action.role_name != "Mossad"
      ret += "#{@user.name} tried to use the #{@action.role_name} role but their action was cancelled and nothing happened."
      return ret
    end

    ret += send(@action.role_name.downcase.gsub(" ", "_").to_sym)
  end

  def bumbling_bureaucrat
    target = @targets[0]
    ret = ""
    target.player_statuses.each do |status|
      next if status.night < @night
      status.update(bamboozled: true)
    end
    ret += "#{@user.name} bumbled #{target.name}"
    ret
  end

  def case_officer
    target = @targets[0]
    target.current_status(@night).update(action_cancelled: true)
    "#{@user.name} is cancelling the action of #{target.name}"
  end

  def hacker
    target = @targets[0]
    stolen_role = target.role
    ret = "#{@user.name} stole #{target.name}'s #{stolen_role.name} role.  #{@user.name} is now a #{stolen_role.name}"
    @user.role = stolen_role
    ret
  end

  def shadchan
    target1 = @targets[0]
    target2 = @targets[1]

    @night.upto 5 do |night|
      target2.current_status(night).player_loved_id = target1.id
      target1.current_status(night).player_loved_id = target2.id
      target1.current_status(night).save
      target2.current_status(night).save
    end
    "#{@user.name} made a match between #{target1.name} and #{target2.name}.  They are now married"
  end
  
  def handler
    target = @targets[0]
    target.current_status(@night).protected = true
    "#{@user.name} protected #{target.name} with the handler role"
  end

  def provocateur
    target = @targets[0]
    if target.role.name == "Babysitter" and not target.current_status(@night).babysitter_auto_use
      @user.current_status(@night).update(laying_low: true)
      @night.upto(5) do |night|
        target.current_status(night).update(babysitter_auto_use: true)
      end
      return "#{@user.name} tried to set up #{target.name}, but they're a babysitter. #{@user.name} is now laying low"

    end

    if target.current_status(@night).protected
      user.current_status(@night).update(laying_low: true)
      return "#{@user.name} tried to set up #{target.name}, but they've been protected.  #{@user.name} is now laying low"
    end

    if @night < 5
      target.current_status(@night+1).set_up = true 
      "#{@user.name} has set up #{target.name}, and they will be compromised tomorrow night"
    else
      "#{@user.name} wanted to set up #{target.name} but the game is ending"
    end
  end

  def handler
    target = @targets[0]
    target.current_status(@night).saved_by_handler = true
    "#{@user.name} is a handler and has saved #{target.name}"
  end

  def babysitter
    target = @targets[0]
    target.current_status(@night).protected = true
    "#{@user.name} is a babysitter and has protected #{target.name}"
  end

  def anarchist
    target = @targets[0]
    "#{@user.name} is an anarchist and has chosen to compromise an agent from #{target.name}"
  end

  def hr_rep
    target = @targets[0]
    ret = "#{@user.name} is an HR Rep and has inquired about #{target.name}"
    if @user.role.name == "Naive HR Rep"
      ret += ".  However, they are a naive hr rep"
    end
    ret
  end

  def un_liaison
    form = JSON.parse @action.form
    target = Group[form['unliaison-group-to-target'].to_i]
    ret = "#{@user.name} is a UN liaison and is granting immunity to #{target.name}"
  end

  def courier
    target1 = @targets[0]
    target2 = @targets[1]
    "#{@user.name} is a courier and would like to deliver information about #{@targets[1].name} to #{@targets[0].name}"
  end

  def bagman
    target = @targets[0]
    target.current_status(@night).saved_by_bagman = true
    "#{@user.name} is a bagman and has saved #{target.name}"
  end

  def decoder
    target = @targets[0]
    "#{@user.name} is a decoder and has targeted the submission of #{target.name}"
  end

  def mossad
    if @user.current_status(@night).dead or @user.current_status(@night).compromised or @user.current_status(@night).exiled
      return ineligible_reason
    end
    target = @targets[0]
    if target.role.name == "Babysitter" and not target.current_status(@night).babysitter_auto_use
      @user.current_status(@night).update(laying_low: true)
      @night.upto(5) do |night|
        target.current_status(night).update(babysitter_auto_use: true)
      end
      return "#{@user.name}, representing the Mossad tried to kill #{target.name}, but they're a babysitter. #{@user.name} is now laying low."
    end

    if target.current_status(@night).protected
      @user.current_status(@night).update(laying_low: true)
      return "#{@user.name}, representing the Mossad, tried to kill #{target.name}, but they've been protected.  #{@user.name} is now laying low"
    end
    
    ret = "#{@user.name}, representing the Mossad, has chosen to kill #{target.name}"
    if target.current_status(@night).saved_by_handler
      ret +=".  However, they have been saved by a handler"
    else
      @night.upto(5) do |n|
        target.current_status(n).dead = true
        target.current_status(n).save
      end
    end
    ret
  end

  def omega
    "#{@user.name} used the omega role"
  end

  #the filter is a list of player_ids that can't be randomly selected
  def get_random_player_target filter = nil
    call = PlayerStatus.where(dead: false).where(night: @night)
    call = call.where(Sequel.~(player_id: filter)) if filter
    call.select(:player_id).all.sample.player_id
  end

  def ineligible_reason
    ret = ''
    if @user.current_status(@night).dead
      ret += "#{@user.name} tried to act as a #{@action.role_name} but they're dead."
      return ret
    end

    if @user.current_status(@night).compromised
      ret += "#{@user.name} tried to act as a #{@action.role_name} but they've been compromised."
      return ret
    end

    if @user.current_status(@night).laying_low
      ret += "#{@user.name} tried to act as a #{@action.role_name} but they're laying low."
      return ret
    end

    if @user.current_status(@night).exiled
      ret += "#{@user.name} tried to act as a #{@action.role_name} but they've been exiled."
      return ret
    end
    
    if @user.current_status(@night).acted
      ret += "#{@user.name} tried to act as a #{@action.role_name} but already did something tonight."
      return ret
    end    
  end

  def right_role
    #it's the correct role if:
    (@action.role_name == @action.player.role.name) or #the names match, or
      (@action.role_name == "Mossad" and @action.player.is_mafia) or #Mafia action
      (@action.role_name == "HR Rep" and @action.player.role.name == "Naive HR Rep")
  end
end
