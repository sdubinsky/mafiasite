Sequel.migration do
  up do
    create_table :player_statuses do
      primary_key :id
      foreign_key :player_id
      Integer :night, null: false
      Bool :bamboozled, default: false #bureaucrat
    end
  end

  down do
    drop_table :player_statuses
  end
end
