Sequel.migration do
  up do
    create_table :roles do
      primary_key :id
      String :name, null: false, size: 500
      String :description, null: false, size: 1000
      Integer :priority, null: false
    end
  end
end
