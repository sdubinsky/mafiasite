Sequel.migration do
  up do
    create_table :roleactions do
      primary_key :id
      Integer :player_id
      String :role_name
      String :form, size: 20000
      DateTime :vote_time
    end
  end

  down do
    drop_table :roleactions
  end
end
