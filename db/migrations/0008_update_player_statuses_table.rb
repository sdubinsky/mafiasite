Sequel.migration do
  up do
    alter_table :player_statuses do
      add_column :in_love, TrueClass, default: false #shadchan
      add_foreign_key :player_loved_id, :players #shadchan
      add_column :action_cancelled, TrueClass, default: false #case officer
      add_column :protected, TrueClass, default: false #babysitter
      add_column :immune, TrueClass, default: false #un liaison
      add_column :compromised, TrueClass, default: false #anarchist/general
      add_column :dead, TrueClass, default: false
      add_column :set_up, TrueClass, default: false #provocateur
      add_column :exiled, TrueClass, default: false
      add_column :laying_low, TrueClass, default: false #babysitter
      add_column :naive, TrueClass, default: false
      add_column :spectating, TrueClass, default: false
    end

    alter_table :players do
      drop_column :alive
      drop_column :active
    end
  end

  down do
    alter_table :player_statuses do
      drop_column :in_love
      drop_foreign_key :player_loved_id 
      drop_column :action_cancelled     
      drop_column :protected            
      drop_column :immune               
      drop_column :compromised          
      drop_column :dead                 
      drop_column :set_up               
      drop_column :exiled               
      drop_column :laying_low           
      drop_column :saved_by_bagman      
      drop_column :saved_by_handler     
      drop_column :naive                
      drop_column :spectating
    end

    alter_table :players do
      add_column :alive, TrueClass, default: true
      add_column :active, TrueClass, default: true
    end
  end
end
