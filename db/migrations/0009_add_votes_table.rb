Sequel.migration do
  change do
    create_table :votes do
      primary_key :id
      Integer :player_id
      String :family_chosen
      String :player_chosen
      DateTime :time
    end
  end
end
