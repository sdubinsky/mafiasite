Sequel.migration do
  up do
    create_table :players do
      primary_key :id
      foreign_key :signup_id
      foreign_key :role_id
      Integer :id_number
      Bool :is_mafia
      Bool :alive
      Bool :active
    end
  end

  down do
    drop_table :players    
  end
end
