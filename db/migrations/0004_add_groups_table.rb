Sequel.migration do
  up do
    create_table :groups do
      primary_key :id
      String :name
    end

    alter_table :players do
      add_foreign_key :group_id, :groups
    end
  end

  down do
    drop_table :groups
    alter_table :players do
      drop_column :group_id
    end
  end
end
