Sequel.migration do
  change do
    alter_table :player_statuses do
      add_column :saved_by_bagman, TrueClass, default: false #bagman
      add_column :saved_by_handler, TrueClass, default: false #handler
    end
  end
end
