Sequel.migration do
  up do
    alter_table :roleactions do
      drop_column :vote_time
      add_column :vote_time, Integer
    end

    alter_table :votes do
      drop_column :time
      add_column :time, Integer
    end
  end

  down do
    alter_table :roleactions do
      drop_column :vote_time
      add_column :vote_time, DateTime
    end

    alter_table :votes do
      drop_column :time
      add_column :time, DateTime
    end
  end
end
