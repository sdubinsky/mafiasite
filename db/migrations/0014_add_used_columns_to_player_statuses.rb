Sequel.migration do
  change do
    alter_table :player_statuses do
      add_column :used_on_self,Integer, default: 0 #babysitter, bagman
      add_column :babysitter_auto_use, TrueClass, default: false
      add_column :used, Integer, default: 0
    end
  end
end
