Sequel.migration do
  change do
    alter_table :player_statuses do
      add_column :acted, TrueClass, default: false
    end
  end
end
