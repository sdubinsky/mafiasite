Sequel.migration do
  up do
    create_table :signups do
      primary_key :id
      String :first_name, null: false
      String :last_name, null: false
      String :email, null: false
      String :special_role, null: false
      String :teammates, null: false
    end
  end

  down do
    drop_table :signups
  end
end
