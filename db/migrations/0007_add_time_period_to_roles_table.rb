Sequel.migration do
  up do
    alter_table :roles do
      add_column :time_period, String
    end
  end

  down do
    alter_table :roles do
      drop_column :time_period
    end
  end
end
