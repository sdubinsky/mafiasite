module Roles
  def bureaucrat user, target
    puts "#{user.name} used the bureaucrat role on #{target.name}.  Target is now bamboozled."
  end
end
