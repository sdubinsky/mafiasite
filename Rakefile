require 'rake/testtask'
require_relative 'db/seed_data'
require_relative 'helpers'
include Helpers
namespace :db do
  db_address = ENV["DATABASE_URL"] || "postgres://localhost/mafiaisrael"
  desc "Run migrations"
  task :migrate, [:version] do |t, args|
    require "sequel/core"
    Sequel.extension :migration
    version = args[:version].to_i if args[:version]
    Sequel.connect(db_address) do |db|
      Sequel::Migrator.run(db, "db/migrations", target: version)
    end
  end

  desc "Add roles to DB"
  task :seed_roles do |t|
    require "sequel"
    Sequel.connect db_address do |db|
      seed_roles db
    end
  end
end

Rake::TestTask.new(:test) do |t|
  t.libs << "test"
  t.test_files =FileList['test/test*.rb']
  t.warning = false
end

namespace :reporting do
  require 'sequel'
  require './action_processor'
  db_address = ENV["DATABASE_URL"] || "postgres://localhost/mafiaisrael"
  desc "count daily votes"
  task :count_votes, :day do |t, args|
    require_relative 'count_votes'
    Sequel.connect db_address do |db|
      require_relative 'models/init'
      puts count_votes db, args[:day]
    end
  end

  desc "Run the current night's actions"
  task :nightactions, :night do |t, args|
    require_relative 'action_processor'
    Sequel.connect db_address do |db|
      require_relative 'models/init'
      puts ActionProcessor.new(args[:night].to_i, db, "N").run
    end
  end

  desc "Run the current day's actions"
  task :dayactions, :day do |t, args|
    require_relative 'action_processor'
    Sequel.connect db_address do |db|
      require_relative 'models/init'
      puts ActionProcessor.new(args[:day].to_i, db, "D").run
    end
  end

  desc "print votes"
  task :print_votes, [:day] do |t, args|
    Sequel.connect db_address do |db|
      require_relative 'models/init'
      if args[:day]
        start_time = day_start_timestamp(args[:day].to_i)
        end_time = day_end_timestamp(args[:day].to_i)
      else
        start_time = 0
        end_time = 1000000000
      end
      puts Vote.where(time: start_time..end_time).map{|v| v.to_s}.join("\n")
    end
  end

  desc "print votes in csv"
task :print_votes_csv, [:day] do |t, args|
    Sequel.connect db_address do |db|
      require_relative 'models/init'
      if args[:day]
        start_time = day_start_timestamp(args[:day].to_i)
        end_time = day_end_timestamp(args[:day].to_i)
      else
        start_time = 0
        end_time = 1000000000
      end
      puts Vote.where(time: start_time..end_time).map{|v| v.to_csv}.join("\n")
    end
  end

  desc "print actions"
  task :print_actions, [:day, :time_period] do |t, args|
    Sequel.connect db_address do |db|
      require_relative 'models/init'
      if args[:day]
        puts args[:day]
        start_time = night_start_timestamp(args[:day].to_i)
        end_time = night_end_timestamp(args[:day].to_i)
      else
        start_time = 0
        end_time = 0
      end
      actions = Roleaction.where(vote_time: start_time..end_time)
      actions = actions.where(Sequel.like(:time_period, "%#{args[:time_period]}")) if args[:time_period]
      puts actions.map{|a| a.to_s}.join("\n")
    end
  end
end
