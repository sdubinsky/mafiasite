module Helpers
  START_TIME = 1533751200 #wednesday night
  def current_night start_timestamp=nil
    start_timestamp ||= START_TIME
    current_time = Time.now.to_i
    diff = current_time - start_timestamp
    days = diff / 60 / 60 / 24
    days += 1
    if days > 3
      days -= 1
    end
    days
  end

  #24 hours later, in seconds
  def night_start_timestamp night
    night += 1 if night > 2
    START_TIME + 24*60*60*(night - 1)
  end

  #11 hours later - cutoff is 7AM
  def night_end_timestamp night
    night_start_timestamp(night) + 11*60*60
  end

  def day_start_timestamp day
    night_start_timestamp(day) + 12*60*60
  end

  def day_end_timestamp day
    day_start_timestamp(day) + 11*60*60
  end
  def current_time timestamp
    Time.strptime(timestamp.to_s, "%s").getlocal("+03:00")
  end
end
