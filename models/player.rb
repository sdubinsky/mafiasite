class Player < Sequel::Model
  many_to_one :signup
  many_to_one :role
  many_to_one :group
  one_to_many :roleactions
  one_to_many :player_statuses
  def name
    signup.first_name.capitalize + " " + signup.last_name.capitalize
  end
  def current_status night
    self.player_statuses_dataset.where(night: night).first
  end
  def votes
    Vote.where(player_id: self.id_number)
  end
end
