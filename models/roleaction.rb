require 'json'
class Roleaction < Sequel::Model
  def player
    Player.first(id_number: player_id)
  end

  def to_s
    user_name = player ? player.name : "no registered player"
    ret = JSON.parse(form).each_pair.map do |h,k|
      begin
        if h.include? "player"
          "targeting #{Player[k.to_i].name} "
        elsif h.include? "group"
          "targeting #{Group[k.to_i].name} "
        end
      rescue
      end
    end.join("and ")
    time = Time.strptime(vote_time.to_s, "%s")
    "ID #{player_id}(#{user_name}) used the #{role_name} role, #{ret}.  They voted at #{time}"
  end
end
