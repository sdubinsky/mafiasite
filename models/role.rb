class Role < Sequel::Model
  one_to_many :players
  def actions night = nil
    if night
      Roleaction.where(role_name: self.name).where(night: night)
    else
      Roleaction.where role_name: self.name
    end
  end
end
