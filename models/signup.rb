class Signup < Sequel::Model
  one_to_one :player
  def formatted
    if special_role == "no"
      role = "They want a special role"
    elsif special_role == "yes"
      role = "They do not want a special role"
    else
      role = "They just want to spectate"
    end
    team = teammates.empty? ? "randoms" : teammates

    "#{first_name} #{last_name} signed up using the email #{email.gsub("@", "[at]")}.  #{role}.  They would like to play with #{team}."
  end
end
