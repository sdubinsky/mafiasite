class Vote < Sequel::Model
  def player
    Player.first id_number: self.player_id
  end

  def valid?
    begin
      self.player.group_id.to_i != family_chosen.to_i and
        self.player.group_id.to_i == Player[player_chosen].group_id.to_i
    rescue
      false
    end
  end

  def to_s
    vote_time = Time.strptime(time.to_s, "%s")
    begin
      player_name = player ? player.name : 'invalid player'
      group_name = Group[family_chosen] ? Group[family_chosen].name : 'invalid group'
      vote_target = Player[player_chosen] ? Player[player_chosen].name : 'invalid vote target'
      "#{player.name} voted for #{Group[family_chosen].name} and #{Player[player_chosen].name}.  This vote was cast at #{vote_time} and is #{valid? ? "valid" : "invalid"}."
    rescue
      "Something is wrong with vote id ##{id} - player id used: #{player_id}. voter: #{player_name}, group chosen: #{group_name}, player chosen: #{vote_target}(#{player_chosen})"
    end
  end

  def to_csv
    vote_time = Time.strptime(time.to_s, "%s")
    begin
      "#{player.name}, #{Group[family_chosen].name}, #{Player[player_chosen].name}, #{vote_time}"
    rescue
    end
  end
end
