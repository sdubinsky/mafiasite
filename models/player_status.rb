class PlayerStatus < Sequel::Model
  many_to_one :player
  def eligible
    !self.dead &&
      !self.compromised &&
      !self.exiled &&
      !self.laying_low &&
      !self.acted
  end
end
