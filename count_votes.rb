require 'sequel'
require 'logger'
require 'json'
require './helpers'

include Helpers
def count_votes db, night
  logger = Logger.new "votes.log"
  start_time = day_start_timestamp(night.to_i)
  end_time = day_end_timestamp(night.to_i)
  voter_ids = Player.all.select{|player| player.current_status(night).eligible}
                .map{|player| player.id_number}
  votes = Vote.where(time: start_time..end_time).where(player_id: voter_ids).all.select{|vote| vote.valid?}.sort{|v| v.time}.uniq{|v| v.player_id}
  ret = ""

  family_votes = Hash.new(0)
  player_votes = Hash.new(0)
  votes.each do |vote|
    family_votes[vote.family_chosen.to_i] += 1
    player_votes[vote.player_chosen.to_i] += 1
  end

  alpha_votes = Role.first(name: "Alpha").actions.where(vote_time: start_time..end_time).sort{|a| a.vote_time}.reverse.uniq{|a| a.player_id}
  omega_votes = Role.first(name: "Omega").actions.where(vote_time: start_time..end_time).sort{|a| a.vote_time}.reverse.uniq{|a| a.player_id}
  alpha_votes.each do |alpha_vote|
    form = JSON.parse alpha_vote.form
    ret += "#{alpha_vote.player.name} is an alpha and voted for "
    ret += "#{Group[form['alpha-second-group-vote'].to_i].name} and "
    ret += "#{Player[form['alpha-second-player-vote'].to_i].name}\n"
    family_votes[form['alpha-second-group-vote'].to_i] += 1
  end
  omega_votes.each do |omega_vote|
    form = JSON.parse omega_vote.form
    ret += "#{omega_vote.player.name} is an omega and voted for "
    ret += "#{Group[form['omega-second-group-vote'].to_i].name} and "
    ret += "#{Player[form['omega-second-player-vote'].to_i].name}\n"
    family_votes[form['omega-second-group-vote'].to_i] -= 1
  end

  #TODO: Filter players by group chosen

  alpha_votes.each do |alpha_vote|
    form = JSON.parse alpha_vote.form
    player_votes[form['alpha-second-player-vote'].to_i] += 1
  end
  omega_votes.each do |omega_vote|
    form = JSON.parse omega_vote.form
    player_votes[form['omega-second-player-vote'].to_i] -= 1
  end

  groups = family_votes.to_a.sort{|a,b| a[1] <=> b[1]}.reverse

  ret += family_votes.each_pair.map{|k, v| "#{Group[k].name}: #{v} vote(s)"}.join("\n")
  ret += "\n\n"
  Group.all.each do |group|
    eligible_player_ids = group.players.map{|p| p.id}
    player_names = group.players.map{|p| p.name}
    eligible_players = player_votes.select{|k, v| eligible_player_ids.include? k}.to_a.sort{|a,b| a[1] <=> b[1]}.reverse
    ret += "For the family #{group.name}:\n"
    ret += eligible_players.map{|k, v| "#{Player[k].name} received #{v} vote(s)"}.join "\n"
    ret += "\n\n"
  end

  non_voters = votes.map{|v| v.player_id}
  ret += "Non-voters: \n"
  non_voters = Player.where(Sequel.~(id_number: non_voters)).map{|a| a.name}.join "\n"
  ret += non_voters
  ret
end
