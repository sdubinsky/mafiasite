require 'minitest/autorun'
require 'rack/test'
require 'sequel'
require 'sqlite3'
require 'logger'
require_relative "../db/seed_data"
require 'pry'
class BaseTest < MiniTest::Test
  include Helpers
  DB = Sequel.sqlite
  def initialize var
    DB.loggers << Logger.new("db.log")
    Sequel.extension :migration
    Sequel::Migrator.run(DB, "db/migrations")
    require "./models/init"
    super
  end

  def run
    DB.transaction(rollback: :always) do
      super
    end
  end

  def assign_role role_name, player
    role = Role.first(name: role_name)
    player.update(role: role)
  end
end
