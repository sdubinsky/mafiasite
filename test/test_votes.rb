require 'pry'
require './count_votes'
require_relative './base_test'

class TestVoting < BaseTest
  def setup
    @groups = 1.upto(5).map{|g| Group.create(name: "g#{g}")}
    @signups = 1.upto(9).map{|s| Signup.create(first_name: "first#{s}", last_name: "last#{s}", email: "asdfads", special_role: "yes", teammates: "")}
    @players = 1.upto(9).map do |p|
      player = Player.create(id_number: "#{p}000000".to_i, is_mafia: false)
      player.signup = @signups[p - 1]
      player
    end

    @players.each do |player|
      1.upto 5 do |night|
        status = PlayerStatus.create(
          night: night,
        )
        status.update(player: player)
      end
    end

    seed_roles DB
  end

  def test_votes_are_counted
    @players[0].group = @groups[0]
    @players[1].group = @groups[1]
    @players[2].group = @groups[0]
    @players[3].group = @groups[1]
    @players[4].group = @groups[2]
    @players[5].group = @groups[2]
    @players.each{|p| p.save}

    Vote.create(player_id: @players[0].id_number, family_chosen: @groups[1].id, player_chosen: @players[2].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[1].id_number, family_chosen: @groups[0].id, player_chosen: @players[3].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[2].id_number, family_chosen: @groups[1].id, player_chosen: @players[0].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[3].id_number, family_chosen: @groups[0].id, player_chosen: @players[1].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[4].id_number, family_chosen: @groups[1].id, player_chosen: @players[5].id, time: day_start_timestamp(1) + 10)

    ret = count_votes DB, 1

    assert ret.include? "#{@players[1].name} received 1 vote(s)"
  end

  def test_alpha_votes_are_counted
    @players[0].group = @groups[0]
    @players[1].group = @groups[1]
    @players[2].group = @groups[0]
    @players[3].group = @groups[1]
    @players[4].group = @groups[2]
    @players[5].group = @groups[2]
    @players.each{|p| p.save}

    Vote.create(player_id: @players[0].id_number, family_chosen: @groups[1].id, player_chosen: @players[2].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[1].id_number, family_chosen: @groups[0].id, player_chosen: @players[3].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[2].id_number, family_chosen: @groups[1].id, player_chosen: @players[0].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[3].id_number, family_chosen: @groups[0].id, player_chosen: @players[1].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[4].id_number, family_chosen: @groups[1].id, player_chosen: @players[5].id, time: day_start_timestamp(1) + 10)


    Roleaction.create(
      player_id: @players[0].id_number,
      role_name: "Alpha",
      form: {"alpha-second-player-vote": "#{@players[1].id}", "alpha-second-group-vote": "#{@groups[2].id}"}.to_json,
      vote_time: day_start_timestamp(1) + 10
    )

    ret = count_votes DB, 1
    
    assert ret.include? "#{@groups[2].name}: 1 vote(s)"
    assert ret.include? "#{@players[1].name} received 2 vote(s)"
  end

  def test_only_picks_player_from_group_chosen
    0.upto 6 do |p|
      @players[p].group = @groups[0]
      @players[p].save
      Vote.create(player_id: @players[p].id_number, family_chosen: @groups[1].id, player_chosen: @players[1].id, time: day_start_timestamp(1) + 10)
    end
    7.upto 8 do |p|
      @players[p].group = @groups[1]
      @players[p].save
      Vote.create(player_id: @players[p].id_number, family_chosen: @groups[0].id, player_chosen: @players[8].id, time: day_start_timestamp(1) + 10)
    end

    ret = count_votes DB, 1
    assert ret.include? "#{@players[8].name} received 2 vote(s)"
  end

  def test_omega_votes_are_counted
    @players[0].group = @groups[0]
    @players[1].group = @groups[1]
    @players[2].group = @groups[0]
    @players[3].group = @groups[1]
    @players[4].group = @groups[2]
    @players[5].group = @groups[2]
    @players.each{|p| p.save}
    #TODO: Second vote must be for someone in your own group
    Vote.create(player_id: @players[0].id_number, family_chosen: @groups[1].id, player_chosen: @players[2].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[1].id_number, family_chosen: @groups[0].id, player_chosen: @players[3].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[2].id_number, family_chosen: @groups[1].id, player_chosen: @players[0].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[3].id_number, family_chosen: @groups[0].id, player_chosen: @players[1].id, time: day_start_timestamp(1) + 10)
    Vote.create(player_id: @players[4].id_number, family_chosen: @groups[1].id, player_chosen: @players[5].id, time: day_start_timestamp(1) + 10)
    assign_role("Omega", @players[0])


    Roleaction.create(
      player_id: @players[0].id_number,
      role_name: "Omega",
      form: {"omega-second-player-vote": "#{@players[1].id}", "omega-second-group-vote": "#{@groups[0].id}"}.to_json,
      vote_time: day_start_timestamp(1) + 10
    )
    ret = count_votes DB, 1

    assert ret.include? "#{@groups[0].name}: 1 vote(s)"
    assert ret.include? "#{@players[1].name} received 0 vote(s)"
  end

  def test_cant_vote_for_your_own_group
    @players[0].update(group: @groups[0])
    v = Vote.create(player_id: @players[0].id_number, family_chosen: @groups[0].id, player_chosen: @players[2].id, time: day_start_timestamp(1) + 10)
    refute v.valid?
  end
  
  def test_can_only_vote_for_your_group_members
    @players[1].update(group: @groups[0])
    @players[2].update(group: @groups[1])
    v = Vote.create(player_id: @players[1].id_number, family_chosen: @groups[0].id, player_chosen: @players[2].id, time: day_start_timestamp(1) + 10)
    refute v.valid?
  end
end
