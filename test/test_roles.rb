require './action_processor'
require_relative 'base_test'
require_relative '../helpers'

class TestRoles < BaseTest
  def setup
    s1 = Signup.create(first_name: "first", last_name: "last", email: "test@", special_role: "yes", teammates: "")
    s2 = Signup.create(first_name: "first2", last_name: "last2", email: "test@", special_role: "yes", teammates: "")
    s3 = Signup.create(first_name: "first3", last_name: "last3", email: "test@", special_role: "yes", teammates: "")

    @p1 = Player.create(id_number: 1000000, is_mafia: false)
    @p2 = Player.create(id_number: 2000000, is_mafia: false)
    @p3 = Player.create(id_number: 3000000, is_mafia: false)

    @p1.update(signup: s1)
    @p2.update(signup: s2)
    @p3.update(signup: s3)

    1.upto 5 do |night|
      status = PlayerStatus.create(
        night: night,
      )
      status.update(player: @p1)
      status2 = PlayerStatus.create(
        night: night
      )
      status2.update(player: @p2)
      status3 = PlayerStatus.create(
        night: night
      )
      status3.update(player: @p3)    
    end

    seed_roles DB

    @start_time = night_start_timestamp(1)
    @end_time = night_end_timestamp(1)
  end

  def teardown
  end
  
  def test_bureaucrat_bumbles
    assign_role "Bumbling Bureaucrat", @p1
    
    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Bumbling Bureaucrat",
      form: {"bureaucrat-player-to-target" => @p2.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )
    results = ActionProcessor.new(1, DB, "N").run @start_time, @end_time
    assert_equal results, "First Last bumbled First2 Last2"
    assert @p2.current_status(1).bamboozled
  end

  def test_bumbled_player_targets_random_person
    assign_role "Bumbling Bureaucrat", @p1
    assign_role "Bumbling Bureaucrat", @p2

    Roleaction.create(
      player_id: @p2.id_number,
      role_name: "Bumbling Bureaucrat",
      form: {"bureaucrat-player-to-target" => @p1.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )
    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Bumbling Bureaucrat",
      form: {"bureaucrat-player-to-target" => @p3.id}.to_json,
      vote_time: night_start_timestamp(1) + 10 + 10
    )

    results = ActionProcessor.new(1, DB, "N").run @start_time, @end_time

    assert results.include? "#{@p1.name} originally targeted #{@p3.name} but was bumbled and is now targeting "
    assert results.include? "#{@p1.name} is no longer bumbled"
  end

  def test_case_officer
    assign_role "Case Officer", @p1
    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Case Officer",
      form: {"case-officer-player-to-target" => @p2.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )

    results = ActionProcessor.new(1, DB, "N").run @start_time, @end_time
    assert_equal results, "First Last is cancelling the action of First2 Last2"
    assert @p2.current_status(1).action_cancelled
  end

  def test_case_officer_cancels_action
    assign_role "Bumbling Bureaucrat", @p1
    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Bumbling Bureaucrat",
      form: {"bureaucrat-player-to-target" => @p2.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )
    @p1.player_statuses.each{|s| s.action_cancelled = true; s.save}
    results = ActionProcessor.new(1, DB, "N").run @start_time, @end_time
    assert_equal results, "First Last tried to use the Bumbling Bureaucrat role but their action was cancelled and nothing happened."
  end

  def test_hacker_steals_role
    assign_role "Hacker", @p1
    assign_role "Bumbling Bureaucrat", @p2

    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Hacker",
      form: {"hacker-player-to-target" => @p2.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )

    results = ActionProcessor.new(1, DB, "N").run @start_time, @end_time
    assert_equal results, "First Last stole First2 Last2's Bumbling Bureaucrat role.  First Last is now a Bumbling Bureaucrat"
  end

  def test_shadchan_marries
    assign_role "Shadchan", @p1

    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Shadchan",
      form: {"shadchan-player-to-target-1" => @p2.id, "shadchan-player-to-target-2" => @p3.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )

    results = ActionProcessor.new(1, DB, "N").run @start_time, @end_time
    assert_equal results, "#{@p1.name} made a match between #{@p2.name} and #{@p3.name}.  They are now married"
    @p2.player_statuses.each do |status|
      assert status.player_loved_id = @p3.id
    end
    @p3.player_statuses.each do |status|
      assert status.player_loved_id = @p3.id
    end
  end

  def test_wrong_id_fails
    assign_role "Bumbling Bureaucrat", @p1
    Roleaction.create(
      player_id: @p1.id_number + 1,
      role_name: "Bumbling Bureaucrat",
      form: {"bureaucrat-player-to-target" => @p2.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )

    results = ActionProcessor.new(1, DB, "N").run @start_time, @end_time
    assert_equal results, "Someone tried to act as a(n) Bumbling Bureaucrat with id 1000001 targeting First2 Last2, but that's not a valid ID"
  end

  def test_wrong_role_fails
    assign_role "Bumbling Bureaucrat", @p1
    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Hacker",
      form: {"hacker-player-to-target" => @p2.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )

    results = ActionProcessor.new(1, DB, "N").run @start_time, @end_time
    assert_equal results, "First Last tried to act as a Hacker targeting First2 Last2, but is actually a(n) Bumbling Bureaucrat."
  end

  def test_user_can_only_act_once
    assign_role "Bumbling Bureaucrat", @p1
    
    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Bumbling Bureaucrat",
      form: {"bureaucrat-player-to-target" => @p2.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )
    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Bumbling Bureaucrat",
      form: {"bureaucrat-player-to-target" => @p3.id}.to_json,
      vote_time: night_start_timestamp(1) + 20
    )
    
    results = ActionProcessor.new(1, DB, "N").run @start_time, @end_time
    
    assert results, "First Last bumbled #{@p3.name}"
    assert @p3.current_status(1).bamboozled  
  end

  def test_babysitter_protects_themselves_once
    assign_role "Babysitter", @p1
    assign_role "Townsperson", @p2
    assign_role "Townsperson", @p3
    @p2.update(is_mafia: true)
    @p3.update(is_mafia: true)
    

    Roleaction.create(
      player_id: @p2.id_number,
      role_name: "Mossad",
      form: {"mossad-player-to-target" => @p1.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )

    results = ActionProcessor.new(1, DB, "N").run @start_time, @end_time

    assert results.include? "#{@p2.name}, representing the Mossad tried to kill #{@p1.name}, but they're a babysitter. #{@p2.name} is now laying low."
    assert @p1.current_status(2).babysitter_auto_use
    Roleaction.create(
      player_id: @p3.id_number,
      role_name: "Mossad",
      form: {"mossad-player-to-target" => @p1.id}.to_json,
      vote_time: night_start_timestamp(2) + 10
    )

    results = ActionProcessor.new(2, DB, "N").run
    assert_equal results, "First3 Last3, representing the Mossad, has chosen to kill First Last"
  end

  def test_mossad_and_regular_role
    assign_role "Babysitter", @p1
    assign_role "Townsperson", @p2
    @p1.update(is_mafia: true)

    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Mossad",
      form: {"mossad-player-to-target" => @p2.id}.to_json,
      vote_time: night_start_timestamp(1) + 10
    )

    Roleaction.create(
      player_id: @p1.id_number,
      role_name: "Babysitter",
      form: {"babysitter-player-to-target" => @p2.id}.to_json,
      vote_time: night_start_timestamp(1) + 20
    )

    results = ActionProcessor.new(1, DB, "N").run
    assert results.include? "First Last, representing the Mossad, has chosen to kill First2 Last2"
  end
end
