require 'sinatra'
require 'sequel'
require 'logger'
require 'json'
require 'bcrypt'

require './helpers'
require './count_votes'
connstr = ENV['DATABASE_URL'] || "postgres://localhost/mafiaisrael"
DB = Sequel.connect connstr
require './models/init'

logger = Logger.new $stdout
logger.level = Logger::INFO
enable :sessions
configure :development do
  set :show_exceptions, true
  logger = Logger.new $stdout
  logger.level = Logger::DEBUG
end

include Helpers
include BCrypt
configure :production do
  
end

get '/' do
  erb :index
end

# get '/signup/?' do
#   erb :signup
# end

get '/action/?' do
  erb :action
end

get '/info/?' do
  erb :info
end

get '/rules/?' do
  erb :rules
end

get '/confirmation/?' do
  erb :confirmation
end

get '/error/?' do
  erb :error
end

get '/dayvote/?' do
  alive_players = PlayerStatus.where(compromised: false, night: current_night, spectating: false, dead: false, exiled: false).select(:player_id)
  @players = Player.where(id: alive_players)
  @groups = Group.where(Sequel.~(name: "None - spectator only")).all
  @day_roles = Role.where(Sequel.like(:time_period, "%D%"))
  erb :dayvote
end

get '/nightvote/?' do
  @roles = Role.where(Sequel.like(:time_period, "%N%")).where{Sequel.~(name: ["Naive HR Rep", "Analyst", "Diplomat"])}.sort{|a,b| a.name <=> b.name}
  erb :nightvote
end

get '/specialactions/:role/?' do
  puts params.to_s
  if not params['role']
    redirect to '/'
  end
  alive_players = PlayerStatus.where(compromised: false, night: current_night, spectating: false, dead: false, exiled: false).select(:player_id)
  @players = Player.where(id: alive_players).sort{|a,b| a.name <=> b.name}
  @groups = Group.all
  begin
    erb params['role'].to_sym
  rescue
    redirect to("/confirmation")
  end
end

# get '/signedup/?' do
#   @signedup = Signup.all.to_a
#   erb :signedup 
# end

# get '/assign_roles/?' do
#   @signups = Signup.all.to_a
#   @roles = Role.all.to_a
#   @groups = Group.all.to_a
#   erb :assign_roles
# end

get '/roster/?' do
  @groups = Group.all.to_a
  erb :roster
end

get '/login/?' do
  erb :login
end

get '/admin/?:day?' do
  logger.info "logged in as #{session[:userid]}"
  unless session[:userid] == User.first(username: 'admin').userid
    redirect to '/login'
  end
  unless params[:day]
    redirect to "/admin/#{current_night}"
  end
  @day = params[:day].to_i
  night_start = night_start_timestamp @day
  night_end = night_end_timestamp @day
  day_start = day_start_timestamp @day
  day_end = day_end_timestamp @day

  @votes = Vote.where(time: day_start..day_end)
  begin
    @counted_votes = count_votes DB, @day
  rescue
    @counted_votes = ""
  end
  @counted_votes = @counted_votes.split("\n")
  @night_actions = Roleaction.where(vote_time: night_start..night_end)
  @day_actions = Roleaction.where(vote_time: day_start..day_end)

  erb :admin
end

# post '/submitsignup/?' do
#   logger.info params.to_s
#   signup = Signup.create(
#     first_name: params['signup-first-name'],
#     last_name: params['signup-last-name'],
#     email: params['signup-email'],
#     special_role: params['signup-special-role'],
#     teammates: params['teammates']
#   ).save
#   redirect to '/'
# end

post '/submitdayvote/?' do
  logger.info params.to_s
  player_chosen = Player[params["dayvote-person"].to_i]
  group_chosen = Group[params['dayvote-family'].to_i]
  if not player_chosen
    logger.error "no player chosen"
    params[:error] = "no player chosen"
    redirect to "/error"
  end

  Vote.create(
    player_id: params['dayvote-id'],
    family_chosen: params['dayvote-family'].to_i,
    player_chosen: player_chosen.id,
    time: Time.now.to_i
  )
  if params['dayvote-role'] == 'None' or
    params['dayvote-role'] == ''
    redirect to '/confirmation'
  end
  session[:id] = params['dayvote-id']
  session[:role] = params['dayvote-role']
  redirect to "/specialactions/#{params['dayvote-role'].downcase.gsub(" ", "")}"
end

post '/submitnightvote/?' do
  logger.info params
  if session[:role] == 'none'
    redirect to "/confirmation"
  else
    session[:id] = params['nightvote-id']
    session[:role] = params['nightvote-role']
    redirect to "/specialactions/#{params['nightvote-role'].downcase.gsub(" ", "")}"
  end
end

# post '/setuproles' do
#   puts params.to_s
#   Signup.each do |signup|
#     name = signup.first_name + " " + signup.last_name
#     role = Role.where(name: params["#{name}-role"]).first
#     group = Group.where(name: params["#{name}-group"]).first
#     is_mafia = !!params["#{name}-mafia"]
#     active = role.name != "spectator"
#     DB.transaction do
#       player = Player.create(
#         id_number: rand(1000000..9999999),
#         is_mafia: is_mafia,
#       )
#       player.update(signup: signup, role: role, group: group)
#       1.upto 5 do |night|
#         status = PlayerStatus.create(
#           night: night,
#           bamboozled: false
#         )
#         status.update(player: player)
#       end
#       logger.info("created #{player.to_s}")
#     end

#   end
#   redirect to('/')
# end

post '/userole' do
  logger.info "ID #{session[:id]} tried to use the #{session[:role]} role, voting: #{params.to_json}"
  action = Roleaction.create(
    player_id: session[:id],
    role_name: session[:role],
    form: params.to_json,
    vote_time: Time.now.to_i
  )

  # player = Player.where(id_number: session[:id]).first
  # unless player.role.name == session[:role]
  #   redirect '/'
  # end
  redirect '/confirmation'
end

post '/authenticate' do
  user = User.first(username: params[:username])
  halt 403 unless user
  password = Password.new user.password_hash
  if password == user.salt + params[:password]
    session[:userid] = user.userid
    redirect to '/admin'
  else
    redirect to '/'
  end
end
